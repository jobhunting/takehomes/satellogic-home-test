import os
import json
from json.decoder import JSONDecodeError
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

from tasks import Task
from logger import Logger


class MajorTom(mqtt.Client):
    response_queue = []
    client_id = os.environ.get("CLIENTID", "majortom")

    def __init__(self, *args, **kwargs):
        super(MajorTom, self).__init__()

        self.on_connect = MajorTom.on_connect_callback
        self.message_callback_add(
            'satellite/%s/tasks' % MajorTom.client_id,
            MajorTom.process_tasks
        )

        self.message_callback_add(
            'spacecontrol/connected',
            MajorTom.process_control_connection
        )

        self.connect(
            os.environ.get("MQTT_HOST", "0.0.0.0"),
            int(os.environ.get("MQTT_PORT", 1883)),
            int(os.environ.get("MQTT_KEEPALIVE", 60))
        )

    @classmethod
    def on_connect_callback(cls, client, userdata, flags, rc):
        Logger.logger().info("Connected with result code " + str(rc))
        client.subscribe("satellite/%s/tasks" % MajorTom.client_id)
        client.subscribe("spacecontrol/connected")
        MajorTom.broadcast_connection()

    @classmethod
    def broadcast_connection(cls):
        Logger.logger().info("Broadcasting connection")
        publish.single(
            'satellite/connected',
            MajorTom.client_id,
            hostname=os.environ.get("MQTT_HOST", "0.0.0.0"),
            port=int(os.environ.get("MQTT_PORT", 1883)),
            keepalive=int(os.environ.get("MQTT_KEEPALIVE", 60))
        )

    @classmethod
    def process_control_connection(cls, client, userdata, message):
        MajorTom.broadcast_connection()

    @classmethod
    def process_tasks(cls, client, userdata, message):
        try:
            tasks = json.loads(message.payload)
        except JSONDecodeError:
            Logger.logger().error('Payload should be JSON')
            return
        Logger.logger().info("Processing tasks")
        for task in tasks:
            task = Task(task)
            task.process()
            MajorTom.response_queue.append(task.raw)
        MajorTom.process_response_queue()

    @classmethod
    def process_response_queue(cls):
        Logger.logger().info('Processing response queue')
        responses = [{
            'topic': 'satellite/%s/responses' % MajorTom.client_id,
            'payload': json.dumps(response)
        } for response in MajorTom.response_queue]
        publish.multiple(
            responses,
            hostname=os.environ.get("MQTT_HOST", "0.0.0.0"),
            port=int(os.environ.get("MQTT_PORT", 1883)),
            keepalive=int(os.environ.get("MQTT_KEEPALIVE", 60))
        )
        Logger.logger().info('Responses queued')
        MajorTom.response_queue = []
