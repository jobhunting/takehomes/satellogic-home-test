from client import MajorTom
from logger import Logger

major = MajorTom()

try:
    major.loop_forever()
except KeyboardInterrupt:
    major.disconnect()
    Logger.logger().info("Disconnected")
