import os
from random import randint


class Task(object):
    def __init__(self, d):
        self.d = d

    def process(self):
        fakeout = int(os.environ.get('RANDOM_FAILURE', 10))
        self.d['succesful'] = randint(0, 100) > fakeout

    @property
    def raw(self):
        return self.d
