# Home Test

Simular el tasking de una flota de satélites.
Para eso debemos crear tres procesos del sistema operativo (arrancados cada uno de manera independientes). Dos para los satelites (`satelite_1` y `satelite_2`), y uno para la estación terrena.

Estos procesos deberán comunicarse entre si, de una manera a definir al resolver el problema.

En base a una lista de tareas que se le pasa a la estación terrena (de una manera a definir), la estación terrena le dice a los satélites que tareas tienen que realizar. (le asigna por ejemplo la mitad de las tareas al satélite 1 y la mitad al satélite 2)

Las tareas tienen los siguientes atributos:
+ nombre:
+ recursos: lista de ids de recursos que necesita una tarea. Estos funcionan como locks excluyentes, un satélite no puede ejecutar dos tareas que usen algún mismo recurso las dos. No hay un límite a la cantidad de recursos que una tarea puede usar o la cantidad de recursos distintos que hay.
+ payoff: cuánto beneficio trae hacer la tarea

Ejemplo:

```python
tarea1 = dict(nombre='fotos', payoff=10, recursos=[1,5])
tarea2 = dict(nombre='mantenimiento', payoff=1, recursos=[1,2])
tarea3 = dict(nombre='pruebas', payoff=1, recursos=[5,6])
tarea4 = dict(nombre='fsck', payoff=0.1, recursos=[1, 6])
```

En este caso una buena asignación sería darle la tarea 1 a el satelite 1 y las 2 y 3 al satélite dos y la tarea 4 no se puede realizar.

La asignación de tareas debe maximizar el "payoff"

Cuando el satélite recibe las tareas debe responder qué tareas pudo realizar y cuáles no en
función a una llamada a random, diciendo que no pudo realizar una tarea el 10% de las veces.

## Setup

## Requerimientos

+ [Docker](https://docs.docker.com/install/)
+ [Docker Compose](https://docs.docker.com/compose/install/)
+ [ahoy-cli](https://github.com/ahoy-cli/ahoy#installation) (opcional, pero recomendado)


## Build de containers

```bash
# Con ahoy
ahoy setup docker build satellite control setup
# Sin ahoy
docker-compose -f docker-compose.yml -f docker-compose.setup.yml -p satellogic-home-test build satellite control setup
```

![](docs/img/container_build.png)

## Precargar dashboard en Grafana

```bash
# Con ahoy
ahoy bootstrap-dashboards
# Sin ahoy
docker-compose -f docker-compose.yml -f docker-compose.setup.yml -p satellogic-home-test up --remove-orphans -d dashboards
docker-compose -f docker-compose.yml -f docker-compose.setup.yml -p satellogic-home-test run setup python /code/create_data_sources.py
docker-compose -f docker-compose.yml -f docker-compose.setup.yml -p satellogic-home-test run setup python /code/create_dashboards.py
```

![](docs/img/dashboard_boostrap.png)

## Operación de flota

### Levantar containers

```bash
# Con ahoy
ahoy docker up
# Sin ahoy
docker-compose -f docker-compose.yml  -p satellogic-home-test up --remove-orphans -d
```

![](docs/img/docker_up.png)

### Activity log

```bash
# Con ahoy
ahoy docker log control satellite satellite2
# Sin ahoy
docker-compose -f docker-compose.yml  -p satellogic-home-test logs --tail=80 --follow control satellite satellite2
```

![](docs/img/docker_log.png)

### Flota expandida

```bash
# Con ahoy
ahoy expanded-fleet docker up
# Sin ahoy
docker-compose -f docker-compose.yml -f docker-compose.dev.yml -f docker-compose.expanded.yml -p satellogic-home-test up --remove-orphans -d
```

![](docs/img/expanded_fleet.png)

### Flota reducida (2 sats)

```bash
# Con ahoy
ahoy reduced-fleet
# Sin ahoy
docker-compose -f docker-compose.yml -f docker-compose.expanded.yml -p satellogic-home-test stop satellite3 satellite4
```
![](docs/img/reduced_fleet.png)

### Mandar plan random

```bash
# Con ahoy
ahoy send-plan
# Sin ahoy
docker exec -it $(docker-compose -f docker-compose.yml  -p satellogic-home-test ps -q control) bash -c "  python /code/spacecontrol/send_sample.py"
```
![](docs/img/send_plan.png)

## Visualización de resultados

Ir a `http://localhost:3000/login` en el browser:

![](docs/img/login.png)

Click en `Task Dashboard`

![](docs/img/home.png)

![](docs/img/dashboard.png)







