import os
import json
from settings import *


def create_dashboards():
    dashboards = GRAFANA_CLIENT.search()
    dashboards = [
        dashboard['uri'] for dashboard in dashboards
        if dashboard['type'] == 'dash-db'
    ]

    d = os.path.join(BASE_DIR, 'dashboards')

    files = [f for f in os.listdir(d)
             if os.path.isfile(os.path.join(d, f))and '.json' in f]

    for f in files:
        print('Loading %s' % f)
        dashboard_uri = 'db/%s' % f.replace('.json', '')
        print('dashboard uri is %s' % dashboard_uri)
        overwrite = dashboard_uri in dashboards
        print('overwrite: %s' % overwrite)
        dashboard = json.load(open(os.path.join(d, f)))
        if overwrite:
            GRAFANA_CLIENT.dashboards.db.create(
                dashboard=dashboard['dashboard'], overwrite=True)
        else:
            if 'id' in dashboard['dashboard'].keys():
                dashboard['dashboard'].pop('id')
            GRAFANA_CLIENT.dashboards.db.create(
                dashboard=dashboard['dashboard'])


if __name__ == "__main__":
    create_dashboards()
