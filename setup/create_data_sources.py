from settings import *

existing_datasources = GRAFANA_CLIENT.datasources()
existing_datasources = [ds['name'] for ds in existing_datasources]

for ds in DATA_SOURCES:
    if not ds['name'] in existing_datasources:
        GRAFANA_CLIENT.datasources.create(**ds)
