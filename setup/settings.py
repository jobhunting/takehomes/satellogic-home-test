import os
from grafana_api_client import GrafanaClient


BASE_DIR = os.path.dirname(os.path.realpath(__file__))

GRAFANA = {
    'host': os.environ.get('GRAFANA_HOST', '0.0.0.0'),
    'user': os.environ.get('GRAFANA_USER', 'admin'),
    'password': os.environ.get('GRAFANA_PASSWORD', 'admin'),
    'port': os.environ.get('GRAFANA_PORT', 3005)
}

DATA_SOURCES = [
    {
        'name': 'influx',
        'type': 'influxdb',
        'access': 'proxy',
        'url': 'http://%s:%s' % (os.environ.get('INFLUXDB_HOST', 'metrics'),
                                 os.environ.get('INFLUXDB_PORT', 8086)),
        'password': os.environ.get('INFLUXDB_USER_PASSWORD', 'admin123'),
        'user': os.environ.get('INFLUXDB_USER', 'admin'),
        'database': os.environ.get('INFLUXDB_DB', 'metrics'),
        'basicAuth': False,
        'isDefault': False,
    },
]

GRAFANA_CLIENT = GrafanaClient((GRAFANA['user'], GRAFANA['password']),
                               host=GRAFANA['host'],
                               port=GRAFANA['port'])
