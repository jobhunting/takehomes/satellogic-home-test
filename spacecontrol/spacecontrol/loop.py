from logger import Logger
from client import SpaceControl

spacecontrol = SpaceControl()

try:
    spacecontrol.loop_forever()
except KeyboardInterrupt:
    spacecontrol.disconnect()
    Logger.logger().info("Disconnected")
